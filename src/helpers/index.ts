import { ActivatedRouteSnapshot, NavigationEnd, Route } from '@angular/router';
import { RichSnippetConfig } from '../rich-snippet.config';

// export function getData(
//   property: string,
//   routeSnapshot: ActivatedRouteSnapshot,
//   config: RichSnippetConfig,
//   defaults: any = {}
// ) {
//   const configData = getConfigData(property, config);
//   const routeData = getRouteData(property, routeSnapshot.firstChild);
//   return { ...configData, ...defaults, ...routeData };
// }

// export function getRouteData(property: string, routeSnapshot: ActivatedRouteSnapshot) {
//   return routeDataExists(property, routeSnapshot)
//     ? routeSnapshot.firstChild.data.richSnippets[property]
//     : {};
// }

// export function routeDataExists(property: string, route: any): boolean {
//   return route.data &&
//     route.data.richSnippets &&
//     route.data.richSnippets[property]
//     ? true
//     : false;
// }

export function getConfigData(property: string, config: RichSnippetConfig) {
  return configDataExists(property, config)
    ? config.defaults[property]
    : {};
}

export function configDataExists(property: string, config: RichSnippetConfig): boolean {
  return config.defaults &&
    config.defaults[property]
    ? true
    : false;
}

export function getDefaultData(property: string, config: RichSnippetConfig) {
  const exists = defaultsExists(property, config);
  return exists && config.defaults[property] instanceof Function
    ? config.defaults[property]()
    : exists
    ? config.defaults[property] 
    : {};
}

export function defaultsExists(property: string, config: RichSnippetConfig): boolean {
  return config.defaults && config.defaults[property] ? true : false;
}

export function getRouteData(property: string, route: ActivatedRouteSnapshot | Route) {
  return routeDataExists(property, route) ? route.data[property] : {};
}

export function routeDataExists(property: string, route: ActivatedRouteSnapshot | Route): boolean {
  return route.data && route.data[property] ? true : false;
}
