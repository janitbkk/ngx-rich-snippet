import { Organization, OrganizationType } from './Organization';
import { TextType } from '../types';

export interface CorporationType extends OrganizationType {
  tickerSymbol?: TextType;
}

export class Corporation extends Organization {
  tickerSymbol: TextType;

  constructor(props: CorporationType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Corporation');
  }
}
