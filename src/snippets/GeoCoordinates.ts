import { Thing } from './Thing';
import {
  TextType,
  PostalAddressType,
  CountryType,
  NumberType
} from '../types';

export interface GeoCoordinatesType {
  address?: PostalAddressType | TextType;
  addressCountry?: CountryType | TextType;
  elevation?: NumberType | TextType;
  latitude?: NumberType | TextType;
  longitude?: NumberType | TextType;
  postalCode?: TextType;
}

export class GeoCoordinates extends Thing {
  address: PostalAddressType | TextType;
  addressCountry: CountryType | TextType;
  elevation: NumberType | TextType;
  latitude: NumberType | TextType;
  longitude: NumberType | TextType;
  postalCode: TextType;

  constructor(props: GeoCoordinatesType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('GeoCoordinates');
  }
}
