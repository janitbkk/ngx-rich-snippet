import { Thing, ThingType } from './Thing';
import { TextType } from '../types';

export interface LanguageType extends ThingType {}

export class Language extends Thing {

  constructor(props: LanguageType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Language');
  }
}
