import { CreativeWork, CreativeWorkType } from './CreativeWork';
import {
  BreadcrumbListType,
  TextType,
  WebPageElementType,
  ImageObjectType,
  OrganizationType,
  PersonType,
  // SpeakableSpecificationType,
  URLType,
  // SpecialtyType,
  DateType
} from '../types';
export type WebPageType = {
  breadcrumb?: BreadcrumbListType,
  lastReviewed?: DateType,
  mainContentOfPage?: WebPageElementType,
  primaryImageOfPage?: ImageObjectType,
  relatedLink?: URLType,
  reviewedBy?: OrganizationType | PersonType,
  significantLink?: URLType,
  speakable?: URLType, // SpeakableSpecificationType
  // specialty: SpecialtyType
} & CreativeWorkType;

export class WebPage extends CreativeWork {
  breadcrumb: BreadcrumbListType;
  lastReviewed: DateType;
  mainContentOfPage: WebPageElementType;
  primaryImageOfPage: ImageObjectType;
  relatedLink: URLType;
  reviewedBy: OrganizationType | PersonType;
  significantLink: URLType;
  speakable: URLType; // SpeakableSpecificationType
  // specialty: SpecialtyType;

  constructor(props: WebPageType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('WebPage');
  }
}
