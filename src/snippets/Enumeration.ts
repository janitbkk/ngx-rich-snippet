import {Thing, ThingType} from './Thing';
import {
	// ClassType,
	// PropertyType
} from '../types';
export type EnumerationType = {
   //  supersededBy?: ClassType | EnumerationType | PropertyType,
    } & ThingType;

export class Enumeration extends Thing {
   //  supersededBy: ClassType | EnumerationType | PropertyType;

    constructor(props: EnumerationType) {
        super(props);
        Object.assign(this, props);
    }

    getType(): string {
        return ('Enumeration');
    }
}
