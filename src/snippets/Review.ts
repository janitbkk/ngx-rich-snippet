import { CreativeWork, CreativeWorkType } from './CreativeWork';
import {
  TextType,
  ThingType,
  RatingType
} from '../types';
export type ReviewType = {
  reviewBody?: TextType,
  itemReviewed?: ThingType,
  reviewRating?: RatingType,
} & CreativeWorkType;

export class Review extends CreativeWork {
  reviewBody: TextType;
  itemReviewed: ThingType;
  reviewRating: RatingType;

  constructor(props: ReviewType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Review');
  }
}
