import { Thing, ThingType } from './Thing';
import {
  TextType,
  PostalAddressType,
  CountryType,
  NumberType
} from '../types';

export interface GeoShapeType extends ThingType {
  address?: PostalAddressType | TextType;
  addressCountry?: CountryType | TextType;
  elevation?: NumberType | TextType;
  box?: TextType;
  circle?: TextType;
  line?: TextType;
  polygon?: TextType;
  postalCode?: TextType;
}

export class GeoShape extends Thing {
  address: PostalAddressType | TextType;
  addressCountry: CountryType | TextType;
  elevation: NumberType | TextType;
  box: TextType;
  circle: TextType;
  line: TextType;
  polygon: TextType;
  postalCode: TextType;

  constructor(props: GeoShapeType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('GeoShape');
  }
}
