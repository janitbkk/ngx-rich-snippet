import { Thing, ThingType } from './Thing';
import {
    LanguageType,
    TextType,
    NumberType,
    // OfferType,
    EventType,
    URLType,
    ProductType,
    AudioObjectType,
    OrganizationType,
    PersonType,
    // InteractionCounterType,
    // AlignmentObjectType,
    DateType,
    DateTimeType,
    // PublicationEventType,
    AggregateRatingType,
    PlaceType,
    MediaObjectType,
    DurationType,
    IntegerType,
    BooleanType,
    // AudienceType,
    ReviewType,
    // CommentType,
    // VideoObjectType
} from '../types';

export interface CreativeWorkType extends ThingType {
    inLanguage?: LanguageType | TextType;
    copyrightYear?: NumberType;
    // offers?: OfferType;
    recordedAt?: EventType;
    material?: URLType | TextType | ProductType;
    accessibilityAPI?: TextType;
    audio?: AudioObjectType;
    translator?: OrganizationType | PersonType;
    // interactionStatistic?: InteractionCounterType;
    publishingPrinciples?: URLType;
    contentRating?: TextType;
    // educationalAlignment?: AlignmentObjectType;
    learningResourceType?: TextType;
    dateModified?: DateType | DateTimeType;
    // releasedEvent?: PublicationEventType,
    aggregateRating?: AggregateRatingType;
    discussionUrl?: URLType;
    citation?: TextType | CreativeWorkType;
    spatialCoverage?: PlaceType;
    character?: PersonType;
    datePublished?: DateType;
    sponsor?: OrganizationType | PersonType;
    encoding?: MediaObjectType;
    timeRequired?: DurationType;
    about?: ThingType;
    sourceOrganization?: OrganizationType;
    workTranslation?: CreativeWorkType;
    commentCount?: IntegerType;
    isBasedOnUrl?: URLType | CreativeWorkType | ProductType;
    temporalCoverage?: TextType | DateTimeType | URLType;
    isFamilyFriendly?: BooleanType;
    license?: URLType | CreativeWorkType;
    associatedMedia?: MediaObjectType;
    mentions?: ThingType;
    award?: TextType;
    locationCreated?: PlaceType;
    copyrightHolder?: OrganizationType | PersonType;
    editor?: PersonType;
    hasPart?: CreativeWorkType;
    creator?: PersonType | OrganizationType;
    headline?: TextType;
    accessibilitySummary?: TextType;
    dateCreated?: DateType | DateTimeType;
    translationOfWork?: CreativeWorkType;
    mainEntity?: ThingType;
    accessibilityFeature?: TextType;
    position?: TextType | IntegerType;
    exampleOfWork?: CreativeWorkType;
    // audience?: AudienceType,
    keywords?: TextType;
    typicalAgeRange?: TextType;
    contentLocation?: PlaceType;
    producer?: OrganizationType | PersonType;
    reviews?: ReviewType;
    educationalUse?: TextType;
    accessMode?: TextType;
    contentReferenceTime?: DateTimeType;
    publisherImprint?: OrganizationType;
    contributor?: OrganizationType | PersonType;
    version?: TextType | NumberType;
    provider?: OrganizationType | PersonType;
    // publication?: PublicationEventType,
    isPartOf?: CreativeWorkType;
    funder?: PersonType | OrganizationType;
    accessibilityControl?: TextType;
    accountablePerson?: PersonType;
    genre?: URLType | TextType;
    thumbnailUrl?: URLType;
    isAccessibleForFree?: BooleanType;
    awards?: TextType;
    // comment?: CommentType;
    publisher?: OrganizationType | PersonType;
    fileFormat?: TextType | URLType;
    isBasedOn?: ProductType | URLType | CreativeWorkType;
    accessibilityHazard?: TextType;
    workExample?: CreativeWorkType;
    author?: PersonType | OrganizationType;
    review?: ReviewType;
    schemaVersion?: URLType | TextType;
    // video?: VideoObjectType;
    accessModeSufficient?: TextType;
    encodings?: MediaObjectType;
    interactivityType?: TextType;
    alternativeHeadline?: TextType;
    text?: TextType;
    }

export class CreativeWork extends Thing {
    inLanguage: LanguageType | TextType;
    copyrightYear: NumberType;
    // offers: OfferType;
    recordedAt: EventType;
    material: URLType | TextType | ProductType;
    accessibilityAPI: TextType;
    audio: AudioObjectType;
    translator: OrganizationType | PersonType;
    // interactionStatistic: InteractionCounterType;
    publishingPrinciples: URLType;
    contentRating: TextType;
    // educationalAlignment: AlignmentObjectType;
    learningResourceType: TextType;
    dateModified: DateType | DateTimeType;
    // releasedEvent: PublicationEventType;
    aggregateRating: AggregateRatingType;
    discussionUrl: URLType;
    citation: TextType | CreativeWorkType;
    spatialCoverage: PlaceType;
    character: PersonType;
    datePublished: DateType;
    sponsor: OrganizationType | PersonType;
    encoding: MediaObjectType;
    timeRequired: DurationType;
    about: ThingType;
    sourceOrganization: OrganizationType;
    workTranslation: CreativeWorkType;
    commentCount: IntegerType;
    isBasedOnUrl: URLType | CreativeWorkType | ProductType;
    temporalCoverage: TextType | DateTimeType | URLType;
    isFamilyFriendly: BooleanType;
    license: URLType | CreativeWorkType;
    associatedMedia: MediaObjectType;
    mentions: ThingType;
    award: TextType;
    locationCreated: PlaceType;
    copyrightHolder: OrganizationType | PersonType;
    editor: PersonType;
    hasPart: CreativeWorkType;
    creator: PersonType | OrganizationType;
    headline: TextType;
    accessibilitySummary: TextType;
    dateCreated: DateType | DateTimeType;
    translationOfWork: CreativeWorkType;
    mainEntity: ThingType;
    accessibilityFeature: TextType;
    position: TextType | IntegerType;
    exampleOfWork: CreativeWorkType;
    // audience: AudienceType;
    keywords: TextType;
    typicalAgeRange: TextType;
    contentLocation: PlaceType;
    producer: OrganizationType | PersonType;
    reviews: ReviewType;
    educationalUse: TextType;
    accessMode: TextType;
    contentReferenceTime: DateTimeType;
    publisherImprint: OrganizationType;
    contributor: OrganizationType | PersonType;
    version: TextType | NumberType;
    provider: OrganizationType | PersonType;
    // publication: PublicationEventType;
    isPartOf: CreativeWorkType;
    funder: PersonType | OrganizationType;
    accessibilityControl: TextType;
    accountablePerson: PersonType;
    genre: URLType | TextType;
    thumbnailUrl: URLType;
    isAccessibleForFree: BooleanType;
    awards: TextType;
    // comment: CommentType;
    publisher: OrganizationType | PersonType;
    fileFormat: TextType | URLType;
    isBasedOn: ProductType | URLType | CreativeWorkType;
    accessibilityHazard: TextType;
    workExample: CreativeWorkType;
    author: PersonType | OrganizationType;
    review: ReviewType;
    schemaVersion: URLType | TextType;
    // video: VideoObjectType;
    accessModeSufficient: TextType;
    encodings: MediaObjectType;
    interactivityType: TextType;
    alternativeHeadline: TextType;
    text: TextType;

    constructor(props: CreativeWorkType) {
        super(props);
        Object.assign(this, props);
    }

    getType(): string {
        return ('CreativeWork');
    }
}
