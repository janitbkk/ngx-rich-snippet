import { Thing, ThingType } from './Thing';
import {
  TimeType,
  DayOfWeekType
} from '../types';

export interface OpeningHoursSpecificationType extends ThingType {
  opens?: TimeType;
  dayOfWeek?: DayOfWeekType;
  closes?: TimeType;
}

export class OpeningHoursSpecification extends Thing {
  opens: TimeType;
  dayOfWeek: DayOfWeekType;
  closes: TimeType;

  constructor(props: OpeningHoursSpecificationType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('OpeningHoursSpecification');
  }
}
