import {Product, ProductType} from './Product';
export type ProductModelType = {
  successorOf?: ProductModelType,
  predecessorOf?: ProductModelType,
  isVariantOf?: ProductModelType,
} & ProductType;

export class ProductModel extends Product {
  successorOf: ProductModelType;
  predecessorOf: ProductModelType;
  isVariantOf: ProductModelType;

  constructor(props: ProductModelType) {
      super(props);
      Object.assign(this, props);
  }

  getType(): string {
      return ('ProductModel');
  }
}
