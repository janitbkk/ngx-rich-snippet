import { CreativeWork, CreativeWorkType } from './CreativeWork';
import { TextType } from '../types';

export interface WebsiteType extends CreativeWorkType {
  issn?: TextType;
}

export class Website extends CreativeWork {
  issn: TextType;

  constructor(props: WebsiteType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Website');
  }
}
