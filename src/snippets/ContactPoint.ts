import { Thing, ThingType } from './Thing';
import {
  ContactPointOptionType,
  ProductType,
  TextType,
  LanguageType
} from '../types';

export interface ContactPointType extends ThingType {
  contactOption?: ContactPointOptionType;
  productSupported?: ProductType | TextType;
  contactType?: TextType;
  telephone?: TextType;
  email?: TextType;
  faxNumber?: TextType;
  availableLanguage?: LanguageType;
}

export class ContactPoint extends Thing {
  contactOption: ContactPointOptionType;
  productSupported: ProductType | TextType;
  contactType: TextType;
  telephone: TextType;
  email: TextType;
  faxNumber: TextType;
  availableLanguage: LanguageType;

  constructor(props: ContactPointType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('ContactPoint');
  }
}

