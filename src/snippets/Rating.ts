import { Thing, ThingType } from './Thing';
import {
  NumberType,
  TextType,
  PersonType,
  OrganizationType
} from '../types';
export interface RatingType extends ThingType {
  bestRating?: NumberType | TextType;
  worstRating?: TextType | NumberType;
  author?: PersonType | OrganizationType;
  ratingValue?: NumberType | TextType;
}

export class Rating extends Thing {
  bestRating: NumberType | TextType;
  worstRating: TextType | NumberType;
  author: PersonType | OrganizationType;
  ratingValue: NumberType | TextType;

  constructor(props: RatingType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Rating');
  }
}
