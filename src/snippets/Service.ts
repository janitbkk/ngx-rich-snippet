import { Thing, ThingType } from './Thing';
import {
  // AudienceType,
  OfferType,
  ImageObjectType,
  URLType,
  // PhysicalActivityCategoryType,
  TextType,
  // OfferCatalogType,
  AggregateRatingType,
  OpeningHoursSpecificationType,
  AdministrativeAreaType,
  PlaceType,
  GeoShapeType,
  BrandType,
  OrganizationType,
  // ServiceChannelType,
  ReviewType,
  PersonType,
  ProductType
} from '../types';

export interface ServiceType extends ThingType {
  //  serviceAudience?: AudienceType;
  offers?: OfferType;
  logo?: ImageObjectType | URLType;
  //  audience?: AudienceType;
  //  category?: ThingType | PhysicalActivityCategoryType | TextType;
  produces?: ThingType;
  termsOfService?: TextType | URLType;
  //  hasOfferCatalog?: OfferCatalogType;
  award?: TextType;
  aggregateRating?: AggregateRatingType;
  hoursAvailable?: OpeningHoursSpecificationType;
  areaServed?: TextType | AdministrativeAreaType | PlaceType | GeoShapeType;
  providerMobility?: TextType;
  brand?: BrandType | OrganizationType;
  //  availableChannel?: ServiceChannelType;
  serviceOutput?: ThingType;
  review?: ReviewType | ReviewType[];
  serviceType?: TextType;
  provider?: PersonType | OrganizationType;
  serviceArea?: PlaceType | GeoShapeType | AdministrativeAreaType;
  isRelatedTo?: ServiceType | ProductType;
  broker?: PersonType | OrganizationType;
  isSimilarTo?: ServiceType | ProductType;
}

export class Service extends Thing {
  // serviceAudience: AudienceType;
  offers: OfferType;
  logo: ImageObjectType | URLType;
  // audience: AudienceType;
  // category: ThingType | PhysicalActivityCategoryType | TextType;
  produces: ThingType;
  termsOfService: TextType | URLType;
  // hasOfferCatalog: OfferCatalogType;
  award: TextType;
  aggregateRating: AggregateRatingType;
  hoursAvailable: OpeningHoursSpecificationType;
  areaServed: TextType | AdministrativeAreaType | PlaceType | GeoShapeType;
  providerMobility: TextType;
  brand: BrandType | OrganizationType;
  // availableChannel: ServiceChannelType;
  serviceOutput: ThingType;
  review: ReviewType | ReviewType[];
  serviceType: TextType;
  provider: PersonType | OrganizationType;
  serviceArea: PlaceType | GeoShapeType | AdministrativeAreaType;
  isRelatedTo: ServiceType | ProductType;
  broker: PersonType | OrganizationType;
  isSimilarTo: ServiceType | ProductType;

  constructor(props: ServiceType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Service');
  }
}
