import { CreativeWork, CreativeWorkType } from './CreativeWork';
import {
  // NewsArticleType,
  URLType,
  TextType,
  DateType,
  BooleanType,
  PlaceType,
  DistanceType,
  // QuantitativeValueType
} from '../types';

export interface MediaObjectType extends CreativeWorkType {
  // associatedArticle?: NewsArticleType,
  contentUrl?: URLType;
  encodesCreativeWork?: CreativeWorkType;
  encodingFormat?: TextType;
  bitrate?: TextType;
  uploadDate?: DateType;
  embedUrl?: URLType;
  playerType?: TextType;
  contentSize?: TextType;
  expires?: DateType;
  requiresSubscription?: BooleanType;
  regionsAllowed?: PlaceType;
  height?: DistanceType; // QuantitativeValueType
  width?: DistanceType; // QuantitativeValueType
}

export class MediaObject extends CreativeWork {
  // associatedArticle: NewsArticleType;
  contentUrl: URLType;
  encodesCreativeWork: CreativeWorkType;
  encodingFormat: TextType;
  bitrate: TextType;
  uploadDate: DateType;
  embedUrl: URLType;
  playerType: TextType;
  contentSize: TextType;
  expires: DateType;
  requiresSubscription: BooleanType;
  regionsAllowed: PlaceType;

  constructor(props: MediaObjectType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('MediaObject');
  }
}
