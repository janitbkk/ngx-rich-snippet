import { Thing, ThingType } from './Thing';
import {
  OrganizationType,
  TextType,
  EventType,
  ContactPointType,
  PlaceType,
  EducationalOrganizationType,
  CountryType,
  // MonetaryAmountType,
  // PriceSpecificationType,
  DateType,
  GenderTypeType,
  URLType
} from '../types';
export interface PersonType extends ThingType {
  worksFor?: OrganizationType;
  additionalName?: TextType;
  performerIn?: EventType;
  jobTitle?: TextType;
  homeLocation?: ContactPointType | PlaceType;
  alumniOf?: OrganizationType | EducationalOrganizationType;
  deathPlace?: PlaceType;
  nationality?: CountryType;
  affiliation?: OrganizationType;
  knows?: PersonType;
  honorificPrefix?: TextType;
  familyName?: TextType;
  follows?: PersonType;
  // netWorth?: MonetaryAmountType | PriceSpecificationType,
  children?: PersonType;
  workLocation?: PlaceType | ContactPointType;
  deathDate?: DateType;
  birthDate?: DateType;
  honorificSuffix?: TextType;
  gender?: GenderTypeType | TextType;
  relatedTo?: PersonType;
  parent?: PersonType;
  colleague?: PersonType | URLType;
  colleagues?: PersonType;
  givenName?: TextType;
  spouse?: PersonType;
  birthPlace?: PlaceType;
  siblings?: PersonType;
  sibling?: PersonType;
  parents?: PersonType;
}

export class Person extends Thing {
  worksFor: OrganizationType;
  additionalName: TextType;
  performerIn: EventType;
  jobTitle: TextType;
  homeLocation: ContactPointType | PlaceType;
  alumniOf: OrganizationType | EducationalOrganizationType;
  deathPlace: PlaceType;
  nationality: CountryType;
  affiliation: OrganizationType;
  knows: PersonType;
  honorificPrefix: TextType;
  familyName: TextType;
  follows: PersonType;
  // netWorth: MonetaryAmountType | PriceSpecificationType;
  children: PersonType;
  workLocation: PlaceType | ContactPointType;
  deathDate: DateType;
  birthDate: DateType;
  honorificSuffix: TextType;
  gender: GenderTypeType | TextType;
  relatedTo: PersonType;
  parent: PersonType;
  colleague: PersonType | URLType;
  colleagues: PersonType;
  givenName: TextType;
  spouse: PersonType;
  birthPlace: PlaceType;
  siblings: PersonType;
  sibling: PersonType;
  parents: PersonType;

  constructor(props: PersonType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Person');
  }
}
