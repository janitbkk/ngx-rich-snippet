import { Thing, ThingType } from './Thing';
import { 
  URLType,
  ImageObjectType, 
  AggregateRatingType, 
  ReviewType 
} from '../types';

export interface BrandType extends ThingType {
  logo?: URLType | ImageObjectType;
  aggregateRating?: AggregateRatingType;
  review?: ReviewType;
}

export class Brand extends Thing {
  logo: URLType | ImageObjectType;
  aggregateRating: AggregateRatingType;
  review: ReviewType;

  constructor(props: BrandType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Brand');
  }
}
