import { Thing, ThingType } from './Thing';
import {
  TextType,
  PersonType,
  PlaceType,
  DateType,
  PostalAddressType,
  ImageObjectType,
  URLType,
  ContactPointType
} from '../types';

export interface OrganizationType extends ThingType {
  department?: OrganizationType;
  leiCode?: TextType;
  employee?: PersonType;
  foundingLocation?: PlaceType;
  foundingDate?: DateType;
  parentOrganization?: OrganizationType;
  dissolutionDate?: DateType;
  employees?: PersonType;
  subOrganization?: OrganizationType;
  legalName?: TextType;
  founders?: PersonType;
  founder?: PersonType;
  email?: TextType;
  telephone?: TextType;
  address?: PostalAddressType | TextType;
  logo?: ImageObjectType | URLType;
  taxID?: TextType;
  location?: PlaceType | PostalAddressType | TextType;
  contactPoint?: ContactPointType;
}

export class Organization extends Thing {
  department: OrganizationType;
  leiCode: TextType;
  employee: PersonType;
  foundingLocation: PlaceType;
  foundingDate: DateType;
  parentOrganization: OrganizationType;
  dissolutionDate: DateType;
  employees: PersonType;
  subOrganization: OrganizationType;
  legalName: TextType;
  founders: PersonType;
  founder: PersonType;
  email: TextType;
  telephone: TextType;
  address: PostalAddressType | TextType;
  logo: ImageObjectType | URLType;
  taxID: TextType;
  location: PlaceType | PostalAddressType | TextType;
  contactPoint: ContactPointType;

  constructor(props: OrganizationType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Organization');
  }
}
