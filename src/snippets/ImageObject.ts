import { MediaObject, MediaObjectType } from './MediaObject';
import {
  TextType,
  PropertyValueType,
  BooleanType
} from '../types';

export interface ImageObjectType extends MediaObjectType {
  caption?: TextType;
  exifData?: PropertyValueType | TextType;
  representativeOfPage?: BooleanType;
  thumbnail?: ImageObjectType;
}

export class ImageObject extends MediaObject {
  caption: TextType;
  exifData: PropertyValueType | TextType;
  representativeOfPage: BooleanType;
  thumbnail: ImageObjectType;

  constructor(props: ImageObjectType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('ImageObject');
  }
}
