import { Thing, ThingType } from './Thing';
import {
  DateType,
  TextType,
  ProductModelType,
  BrandType,
  OrganizationType,
  OfferType,
  ReviewType,
  AggregateRatingType
} from '../types';

export interface ProductType extends ThingType {
  releaseDate?: DateType;
  productID?: TextType;
  isConsumableFor?: ProductType;
  isAccessoryOrSparePartFor?: ProductType;
  model?: ProductModelType | TextType;
  color?: TextType;
  brand?: BrandType | OrganizationType;
  offers?: OfferType;
  review?: ReviewType | ReviewType[];
  aggregateRating?: AggregateRatingType;
}

export class Product extends Thing {
  releaseDate: DateType;
  productID: TextType;
  isConsumableFor: ProductType;
  isAccessoryOrSparePartFor: ProductType;
  model: ProductModelType | TextType;
  color: TextType;
  brand: BrandType | OrganizationType;
  offers: OfferType;
  review: ReviewType | ReviewType[];
  aggregateRating: AggregateRatingType;

  constructor(props: ProductType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Product');
  }
}
