import { Thing, ThingType } from './Thing';
import {
  URLType,
  OpeningHoursSpecificationType,
  ImageObjectType,
  GeoCoordinatesType,
  GeoShapeType,
  TextType,
  GeospatialGeometryType,
  PhotographType,
  PostalAddressType,
  IntegerType,
  AggregateRatingType,
  ReviewType,
  PropertyValueType,
  BooleanType,
  MapType,
  EventType
} from '../types';

export interface PlaceType extends ThingType {
  maps?: URLType;
  specialOpeningHoursSpecification?: OpeningHoursSpecificationType;
  logo?: ImageObjectType | URLType;
  geo?: GeoCoordinatesType | GeoShapeType;
  map?: URLType;
  faxNumber?: TextType;
  geospatiallyDisjoint?: GeospatialGeometryType | PlaceType;
  geospatiallyCrosses?: PlaceType | GeospatialGeometryType;
  photos?: ImageObjectType | PhotographType;
  address?: TextType | PostalAddressType;
  containedIn?: PlaceType;
  geospatiallyTouches?: GeospatialGeometryType | PlaceType;
  globalLocationNumber?: TextType;
  maximumAttendeeCapacity?: IntegerType;
  openingHoursSpecification?: OpeningHoursSpecificationType | OpeningHoursSpecificationType[];
  aggregateRating?: AggregateRatingType;
  reviews?: ReviewType;
  additionalProperty?: PropertyValueType;
  geospatiallyContains?: PlaceType | GeospatialGeometryType;
  geospatiallyOverlaps?: PlaceType | GeospatialGeometryType;
  branchCode?: TextType;
  geospatiallyCoveredBy?: PlaceType | GeospatialGeometryType;
  photo?: PhotographType | ImageObjectType;
  containsPlace?: PlaceType;
  telephone?: TextType;
  review?: ReviewType;
  containedInPlace?: PlaceType;
  smokingAllowed?: BooleanType;
  hasMap?: MapType | URLType;
  geospatiallyWithin?: GeospatialGeometryType | PlaceType;
  geospatiallyCovers?: GeospatialGeometryType | PlaceType;
  events?: EventType;
  isicV4?: TextType;
  geospatiallyIntersects?: PlaceType | GeospatialGeometryType;
  event?: EventType;
  geospatiallyEquals?: GeospatialGeometryType | PlaceType;
}

export class Place extends Thing {
  maps: URLType;
  specialOpeningHoursSpecification: OpeningHoursSpecificationType;
  logo: ImageObjectType | URLType;
  geo: GeoCoordinatesType | GeoShapeType;
  map: URLType;
  faxNumber: TextType;
  geospatiallyDisjoint: GeospatialGeometryType | PlaceType;
  geospatiallyCrosses: PlaceType | GeospatialGeometryType;
  photos: ImageObjectType | PhotographType;
  address: TextType | PostalAddressType;
  containedIn: PlaceType;
  geospatiallyTouches: GeospatialGeometryType | PlaceType;
  globalLocationNumber: TextType;
  maximumAttendeeCapacity: IntegerType;
  openingHoursSpecification: OpeningHoursSpecificationType | OpeningHoursSpecificationType[];
  aggregateRating: AggregateRatingType;
  reviews: ReviewType;
  additionalProperty: PropertyValueType;
  geospatiallyContains: PlaceType | GeospatialGeometryType;
  geospatiallyOverlaps: PlaceType | GeospatialGeometryType;
  branchCode: TextType;
  geospatiallyCoveredBy: PlaceType | GeospatialGeometryType;
  photo: PhotographType | ImageObjectType;
  containsPlace: PlaceType;
  telephone: TextType;
  review: ReviewType;
  containedInPlace: PlaceType;
  smokingAllowed: BooleanType;
  hasMap: MapType | URLType;
  geospatiallyWithin: GeospatialGeometryType | PlaceType;
  geospatiallyCovers: GeospatialGeometryType | PlaceType;
  events: EventType;
  isicV4: TextType;
  geospatiallyIntersects: PlaceType | GeospatialGeometryType;
  event: EventType;
  geospatiallyEquals: GeospatialGeometryType | PlaceType;

  constructor(props: PlaceType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Place');
  }
}
