import { Thing, ThingType } from './Thing';
import {
  IntegerType,
  ItemListOrderTypeType,
  TextType,
  ListItemType
} from '../types';

export interface BreadcrumbListType extends ThingType {
  numberOfItems?: IntegerType;
  itemListOrder?: ItemListOrderTypeType | TextType;
  itemListElement: Array<ListItemType | TextType | ThingType>;
}

export class BreadcrumbList extends Thing {
  numberOfItems: IntegerType;
  itemListOrder: ItemListOrderTypeType | TextType;
  itemListElement: Array<ListItemType | TextType | ThingType>;

  constructor(props: BreadcrumbListType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('BreadcrumbList');
  }
}
