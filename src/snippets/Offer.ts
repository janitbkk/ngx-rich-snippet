import { Thing, ThingType } from './Thing';
import {
  DateType,
  DeliveryMethodType,
  ProductType,
  ServiceType,
  // QuantitativeValueType,
  TextType,
  // ItemAvailabilityType,
  NumberType,
  OrganizationType,
  PersonType,
  // LoanOrCreditType,
  // PaymentMethodType,
  PlaceType,
  DateTimeType,
  GeoShapeType,
  // PriceSpecificationType,
  // BusinessFunctionType,
  AggregateRatingType,
  ReviewType,
  AdministrativeAreaType,
  // OfferItemConditionType,
  // WarrantyPromiseType,
  // PhysicalActivityCategoryType,
  // TypeAndQuantityNodeType,
  // BusinessEntityTypeType
} from '../types';

export interface OfferType extends ThingType {
  priceValidUntil?: DateType;
  availableDeliveryMethod?: DeliveryMethodType;
  itemOffered?: ProductType | ServiceType;
  //  eligibleQuantity?: QuantitativeValueType;
  serialNumber?: TextType;
  //  availability?: ItemAvailabilityType;
  //  eligibleDuration?: QuantitativeValueType;
  price?: NumberType | TextType;
  offeredBy?: OrganizationType | PersonType;
  //  acceptedPaymentMethod?: LoanOrCreditType | PaymentMethodType;
  seller?: OrganizationType | PersonType;
  availableAtOrFrom?: PlaceType;
  validFrom?: DateTimeType;
  mpn?: TextType;
  eligibleRegion?: TextType | PlaceType | GeoShapeType;
  //  priceSpecification?: PriceSpecificationType;
  //  deliveryLeadTime?: QuantitativeValueType;
  //  businessFunction?: BusinessFunctionType;
  aggregateRating?: AggregateRatingType;
  reviews?: ReviewType;
  sku?: TextType;
  //  eligibleTransactionVolume?: PriceSpecificationType;
  availabilityStarts?: DateTimeType;
  areaServed?: GeoShapeType | TextType | AdministrativeAreaType | PlaceType;
  ineligibleRegion?: PlaceType | GeoShapeType | TextType;
  //  advanceBookingRequirement?: QuantitativeValueType;
  gtin12?: TextType;
  gtin13?: TextType;
  gtin14?: TextType;
  priceCurrency?: TextType;
  review?: ReviewType;
  availabilityEnds?: DateTimeType;
  //  itemCondition?: OfferItemConditionType;
  //  warranty?: WarrantyPromiseType;
  //  category?: ThingType | TextType | PhysicalActivityCategoryType;
  //  includesObject?: TypeAndQuantityNodeType;
  validThrough?: DateTimeType;
  addOn?: OfferType;
  //  eligibleCustomerType?: BusinessEntityTypeType;
  gtin8?: TextType;
  //  inventoryLevel?: QuantitativeValueType;
}

export class Offer extends Thing {
  priceValidUntil: DateType;
  availableDeliveryMethod: DeliveryMethodType;
  itemOffered: ProductType | ServiceType;
  //  eligibleQuantity: QuantitativeValueType;
  serialNumber: TextType;
  //  availability: ItemAvailabilityType;
  //  eligibleDuration: QuantitativeValueType;
  price: NumberType | TextType;
  offeredBy: OrganizationType | PersonType;
  //  acceptedPaymentMethod: LoanOrCreditType | PaymentMethodType;
  seller: OrganizationType | PersonType;
  availableAtOrFrom: PlaceType;
  validFrom: DateTimeType;
  mpn: TextType;
  eligibleRegion: TextType | PlaceType | GeoShapeType;
  //  priceSpecification: PriceSpecificationType;
  //  deliveryLeadTime: QuantitativeValueType;
  //  businessFunction: BusinessFunctionType;
  aggregateRating: AggregateRatingType;
  reviews: ReviewType;
  sku: TextType;
  //  eligibleTransactionVolume: PriceSpecificationType;
  availabilityStarts: DateTimeType;
  areaServed: GeoShapeType | TextType | AdministrativeAreaType | PlaceType;
  ineligibleRegion: PlaceType | GeoShapeType | TextType;
  //  advanceBookingRequirement: QuantitativeValueType;
  gtin12: TextType;
  gtin13: TextType;
  gtin14: TextType;
  priceCurrency: TextType;
  review: ReviewType;
  availabilityEnds: DateTimeType;
  //  itemCondition: OfferItemConditionType;
  //  warranty: WarrantyPromiseType;
  //  category: ThingType | TextType | PhysicalActivityCategoryType;
  //  includesObject: TypeAndQuantityNodeType;
  validThrough: DateTimeType;
  addOn: OfferType;
  //  eligibleCustomerType: BusinessEntityTypeType;
  gtin8: TextType;
  //  inventoryLevel: QuantitativeValueType;

  constructor(props: OfferType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Offer');
  }
}
