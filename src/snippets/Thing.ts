import { Context } from './Context';
import {
  ContextType,
  TextType,
  CreativeWorkType,
  EventType,
  URLType,
  ImageObjectType,
  ActionType,
  PropertyValueType
} from '../types';
export type ThingType = {
  disambiguatingDescription?: TextType,
  subjectOf?: CreativeWorkType | EventType,
  sameAs?: URLType | URLType[],
  alternateName?: TextType,
  image?: ImageObjectType | URLType,
  mainEntityOfPage?: CreativeWorkType | URLType,
  potentialAction?: ActionType,
  description?: TextType,
  name?: TextType  | TextType[],
  identifier?: URLType | PropertyValueType | TextType,
  url?: URLType,
  additionalType?: URLType,
} & ContextType;

export class Thing extends Context {
  disambiguatingDescription: TextType;
  subjectOf: CreativeWorkType | EventType;
  sameAs: URLType;
  alternateName: TextType;
  image: ImageObjectType | URLType;
  mainEntityOfPage: CreativeWorkType | URLType;
  potentialAction: ActionType;
  description: TextType;
  name: TextType | TextType[];
  identifier: URLType | PropertyValueType | TextType;
  url: URLType;
  additionalType: URLType;

  constructor(props: ThingType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Thing');
  }
}
