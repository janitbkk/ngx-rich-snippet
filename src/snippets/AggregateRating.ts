import { Rating, RatingType } from './Rating';
import {
  ThingType,
  IntegerType
} from '../types';

export interface AggregateRatingType extends RatingType {
  itemReviewed?: ThingType;
  ratingCount?: IntegerType;
  reviewCount?: IntegerType;
}

export class AggregateRating extends Rating {
  itemReviewed: ThingType;
  ratingCount: IntegerType;
  reviewCount: IntegerType;

  constructor(props: AggregateRatingType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('AggregateRating');
  }
}
