import { CreativeWork, CreativeWorkType } from './CreativeWork';
import { MapCategoryTypeType } from '../types';
export type MapType = {
  mapType?: MapCategoryTypeType,
} & CreativeWorkType;

export class Map extends CreativeWork {
  mapType: MapCategoryTypeType;

  constructor(props: MapType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Map');
  }
}
