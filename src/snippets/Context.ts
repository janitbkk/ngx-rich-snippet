export const DEFAULT_CONTEXT = 'http://schema.org';

export class Context {
  constructor(props: any) {
    if (!this.getType) {
      throw new TypeError('Subclass must implement the <getType> method');
    }
  }

  toJSON() {
    return Object.assign({},
      {
        '@type': this.getType(),
        '@context': DEFAULT_CONTEXT
      },
      this);
  }

  getType(): string {
    throw new Error('Attempted to call abstract method <getType>');
  }
}
