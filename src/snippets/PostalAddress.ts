import { ContactPoint, ContactPointType } from './ContactPoint';
import {
  TextType,
  CountryType
} from '../types';

export interface PostalAddressType extends ContactPointType {
  streetAddress?: TextType;
  addressLocality?: TextType;
  addressRegion?: TextType;
  postOfficeBoxNumber?: TextType;
  postalCode?: TextType;
  addressCountry?: CountryType | TextType;
}

export class PostalAddress extends ContactPoint {
  streetAddress: TextType;
  addressLocality: TextType;
  addressRegion: TextType;
  postOfficeBoxNumber: TextType;
  postalCode: TextType;
  addressCountry: CountryType | TextType;

  constructor(props: PostalAddressType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('PostalAddress');
  }
}
