import {Thing, ThingType} from './Thing';
import {
	TextType,
	IntegerType
} from '../types';
export type ListItemType = {
    nextItem?: ListItemType,
    position?: TextType | IntegerType,
    item?: ThingType,
    previousItem?: ListItemType,
    } & ThingType;

export class ListItem extends Thing {
    nextItem: ListItemType;
    position: TextType | IntegerType;
    item: ThingType;
    previousItem: ListItemType;

    constructor(props: ListItemType) {
        super(props);
        Object.assign(this, props);
    }

    getType(): string {
        return ('ListItem');
    }
}
