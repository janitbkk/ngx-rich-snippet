import { Thing, ThingType } from './Thing';
import {
  CreativeWorkType,
  IntegerType,
  OrganizationType,
  PersonType,
  DateType,
  DateTimeType,
  EventStatusTypeType
} from '../types';
export type EventType = {
  recordedIn?: CreativeWorkType,
  subEvents?: EventType,
  remainingAttendeeCapacity?: IntegerType,
  performers?: OrganizationType | PersonType,
  workPerformed?: CreativeWorkType,
  superEvent?: EventType,
  organizer?: OrganizationType | PersonType,
  performer?: OrganizationType | PersonType,
  previousStartDate?: DateType,
  doorTime?: DateTimeType,
  subEvent?: EventType,
  workFeatured?: CreativeWorkType,
  eventStatus?: EventStatusTypeType,
  attendee?: OrganizationType | PersonType,
  attendees?: OrganizationType | PersonType,
} & ThingType;

export class Event extends Thing {
  recordedIn: CreativeWorkType;
  subEvents: EventType;
  remainingAttendeeCapacity: IntegerType;
  performers: OrganizationType | PersonType;
  workPerformed: CreativeWorkType;
  superEvent: EventType;
  organizer: OrganizationType | PersonType;
  performer: OrganizationType | PersonType;
  previousStartDate: DateType;
  doorTime: DateTimeType;
  subEvent: EventType;
  workFeatured: CreativeWorkType;
  eventStatus: EventStatusTypeType;
  attendee: OrganizationType | PersonType;
  attendees: OrganizationType | PersonType;

  constructor(props: EventType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('Event');
  }
}
