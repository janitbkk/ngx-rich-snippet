import { CreativeWork, CreativeWorkType } from './CreativeWork';
import {
  XPathType,
  CssSelectorType,
} from '../types';
export type WebPageElementType = {
  cssSelector?: CssSelectorType,
  xpath?: XPathType,
} & CreativeWorkType;

export class WebPageElement extends CreativeWork {
  cssSelector: CssSelectorType;
  xpath: XPathType;

  constructor(props: WebPageElementType) {
    super(props);
    Object.assign(this, props);
  }

  getType(): string {
    return ('WebPageElement');
  }
}
