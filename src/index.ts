import { NgModule, ModuleWithProviders, Provider } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RICH_SNIPPET_CONFIG, RichSnippetConfig } from './rich-snippet.config';
import { RichSnippetService } from './services/rich-snippet.service';

export * from './services/web-page.service';
export * from './services/breadcrumb.service';
export * from './rich-snippet.config';
export * from './types/index';
export * from './snippets/index';

@NgModule({
  imports: [RouterModule]
})
export class RichSnippetModule {

  static forRoot(richSnippetConfig: RichSnippetConfig, providers: Provider[]): ModuleWithProviders {
    return {
      ngModule: RichSnippetModule,
      providers: [
        { provide: RICH_SNIPPET_CONFIG, useValue: richSnippetConfig },
        { provide: RichSnippetService,
          useFactory: RichSnippetServiceFactory,
          deps: [RICH_SNIPPET_CONFIG]
        },
        ...providers
      ]
    };
  }
}

export function RichSnippetServiceFactory(config) {
  return new RichSnippetService(config);
}
