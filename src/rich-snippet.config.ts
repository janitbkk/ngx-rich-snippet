import { InjectionToken } from '@angular/core';
import { Route } from '@angular/router';
import {
  CorporationType,
  ImageObjectType
} from './types';

export const RICH_SNIPPET_CONFIG = new InjectionToken<RichSnippetConfig>('RichSnippetToken');

export interface RichSnippetConfig {
  baseUrl: string;
  defaults?: any;
}