
export {ThingType} from '../snippets/Thing';
export {CreativeWorkType} from '../snippets/CreativeWork';
// export {VideoObjectType} from '../snippets/VideoObject';
// export {NewsArticleType} from '../snippets/NewsArticle';
// export {InteractionCounterType} from '../snippets/InteractionCounter';
export {MediaObjectType} from '../snippets/MediaObject';
export {ProductType} from '../snippets/Product';
export {ProductModelType} from '../snippets/ProductModel';
export {PlaceType} from '../snippets/Place';
// export {SoftwareApplicationType} from '../snippets/SoftwareApplication';
export {OrganizationType} from '../snippets/Organization';
export {PersonType} from '../snippets/Person';
// export {PublicationEventType} from '../snippets/PublicationEvent';
// export {AlignmentObjectType} from '../snippets/alignment-object';
export {EventType} from '../snippets/Event';
export {ContactPointType} from '../snippets/ContactPoint';
// export {DataFeedType} from '../snippets/data-feed';
// export {BroadcastServiceType} from '../snippets/broadcast-service';
export {GeoCoordinatesType} from '../snippets/GeoCoordinates';
export {GeoShapeType} from '../snippets/GeoShape';
export {MapType} from '../snippets/Map';
// export {BroadcastChannelType} from '../snippets/broadcast-channel';
// export {ServiceChannelType} from '../snippets/service-channel';
// export {AudienceType} from '../snippets/audience';
export {OpeningHoursSpecificationType} from '../snippets/OpeningHoursSpecification';
// export {DataFeedItemType} from '../snippets/data-feed-item';
// export {DataCatalogType} from '../snippets/data-catalog';
// export {PriceSpecificationType} from '../snippets/price-specification';
export {PostalAddressType} from '../snippets/PostalAddress';
// export {DatasetType} from '../snippets/dataset';
// export {QuantitativeValueType} from '../snippets/quantitative-value';
// export {LocationFeatureSpecificationType} from '../snippets/location-feature-specification';
export {GeospatialGeometryType} from '../snippets/GeospatialGeometry';
export {ReviewType} from '../snippets/Review';
// export {QualitativeValueType} from '../snippets/qualitative-value';
export {AggregateRatingType} from '../snippets/AggregateRating';
// export {MonetaryAmountType} from '../snippets/monetary-amount';
export {RatingType} from '../snippets/Rating';
// export {TypeAndQuantityNodeType} from '../snippets/type-and-quantity-node';
// export {ClassType} from '../snippets/class';
// export {PropertyType} from '../snippets/property';
// export {CommentType} from '../snippets/comment';
// export {QuestionType} from '../snippets/question';
// export {LoanOrCreditType} from '../snippets/loan-or-credit';
// export {WarrantyPromiseType} from '../snippets/warranty-promise';
// export {BroadcastFrequencySpecificationType} from '../snippets/broadcast-frequency-specification';
// export {RepaymentSpecificationType} from '../snippets/repayment-specification';
export {ListItemType} from '../snippets/ListItem';
// export {SingleFamilyResidenceType} from '../snippets/single-family-residence';
// export {ItemListType} from '../snippets/item-list';
export {BreadcrumbListType} from '../snippets/BreadcrumbList';
export {WebPageType} from '../snippets/WebPage';
export {WebPageElementType} from '../snippets/WebPageElement';
export {ImageObjectType} from '../snippets/ImageObject';
export {WebsiteType} from '../snippets/Website';
export {LanguageType} from '../snippets/Language';
export {CorporationType} from '../snippets/Corporation';
export {ServiceType} from '../snippets/Service';
export {OfferType} from '../snippets/Offer';
export {BrandType} from '../snippets/Brand';

export type DurationType = string;
export type DateType = string;
export type DateTimeType = string;

export type URLType = string;
export interface ContextType {}

export type TextType = string;
export type DistanceType = number | string;
export type XPathType = string;
export type CssSelectorType = string;
export type ActionType = string;
export type PropertyValueType = string;
export type NumberType = number;
export type IntegerType = number;
export type BooleanType = boolean;
export type EventStatusTypeType = string;
export type GenderTypeType = string;
export type ContactPointOptionType = string;
export type TimeType = string; // ISO-8601 time-of-day
export type DayOfWeekType =
    'Sunday'
    | 'Monday'
    | 'Tuesday'
    | 'Wednesday'
    | 'Thursday'
    | 'Friday'
    | 'Saturday';
export type MapCategoryTypeType =
    'ParkingMap'
    | 'SeatingMap'
    | 'TransitMap'
    | 'VenueMap';

// FIXME there should be classes snippets for at least some of these.
import {CreativeWorkType} from '../snippets/CreativeWork';
export type WebSiteType = CreativeWorkType;
export type AudioObjectType = CreativeWorkType;
export type PhotographType = CreativeWorkType;

import {PlaceType} from '../snippets/Place';
export type CountryType = PlaceType;
export type AdministrativeAreaType = PlaceType;

// import {ServiceType} from '../snippets/Service';
// export type CableOrSatelliteServiceType = ServiceType;

import {MediaObjectType} from '../snippets/MediaObject';
export type DataDownloadType = MediaObjectType;

import {OrganizationType} from '../snippets/Organization';
export type EducationalOrganizationType = OrganizationType;

import {ThingType} from '../snippets/Thing';
export type StructuredValueType = ThingType;

import {EnumerationType} from '../snippets/Enumeration';
// export type PhysicalActivityCategoryType = EnumerationType;
// export type BusinessEntityTypeType = EnumerationType;
// export type BusinessFunctionType = EnumerationType;
export type DeliveryMethodType = EnumerationType;
// export type ItemAvailabilityType = EnumerationType;
// export type PaymentMethodType = EnumerationType;
// export type OfferItemConditionType = EnumerationType;
export type ItemListOrderTypeType = EnumerationType;
// export type WarrantyScopeType = WarrantyScopeType;

// import {CommentType} from '../snippets/comment';
// export type AnswerType = CommentType;

// import {ItemListType} from '../snippets/ItemList';
// export type OfferCatalogType = ItemListType;
