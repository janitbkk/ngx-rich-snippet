import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { RICH_SNIPPET_CONFIG, RichSnippetConfig } from '../rich-snippet.config';

@Injectable()
export class RichSnippetService {
  private _config: BehaviorSubject<any> = new BehaviorSubject<any>({});
  readonly config: Observable<any> = this._config.asObservable();

  constructor(
    @Inject(RICH_SNIPPET_CONFIG) private options: RichSnippetConfig
  ) {
    this._config.next(this.resolveConfig(options));
  }

  private resolveConfig(config: RichSnippetConfig) {
    return config;
  }
}
