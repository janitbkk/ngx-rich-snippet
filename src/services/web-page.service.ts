import { Injectable} from '@angular/core';
import { 
  Router, 
  ActivatedRoute, 
  ActivatedRouteSnapshot, 
  NavigationEnd 
} from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import { 
  getRouteData, 
  getConfigData, 
  getDefaultData 
} from '../helpers/index';
import { RichSnippetConfig } from '../rich-snippet.config';
import { WebPage } from '../snippets/index';
import { RichSnippetService } from './rich-snippet.service';

const CONFIG_PROPERTY = 'rsWebPage';

@Injectable()
export class RsWebPageService {
  private _current: BehaviorSubject<any> = new BehaviorSubject<any>({});
  readonly current: Observable<any> = this._current.asObservable();

  private config: RichSnippetConfig;

  constructor(
    private richSnippetService: RichSnippetService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {

    richSnippetService.config.subscribe(val => this.config = val);

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((route: NavigationEnd) => this.routeChange(this.activatedRoute.snapshot, route));
  }

  routeChange(routeSnapshot: ActivatedRouteSnapshot, route: NavigationEnd) {
    const defaultData = getDefaultData(CONFIG_PROPERTY, this.config);
    const routeData = getRouteData(CONFIG_PROPERTY, routeSnapshot);
    const webPage = new WebPage({ 
      ...this.webPageDefaults(routeSnapshot, route), 
      ...defaultData, 
      ...routeData 
    });

    this._current.next(webPage.toJSON());
  }

  private webPageDefaults(routeSnapshot: ActivatedRouteSnapshot, route: NavigationEnd) {
    return {
      inLanguage: 'en_us',
      copyrightYear: new Date().getFullYear(),
      url: this.config.baseUrl + route.urlAfterRedirects
    }
  }
}
