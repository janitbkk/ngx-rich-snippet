import { Injectable } from '@angular/core';
import {
  Router,
  ActivatedRoute,
  ActivatedRouteSnapshot,
  NavigationEnd
} from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { getRouteData, routeDataExists } from '../helpers/index';
import { RichSnippetConfig } from '../rich-snippet.config';
import { BreadcrumbList, ListItem } from '../snippets/index';

import { RichSnippetService } from './rich-snippet.service';

const DATA_PROPERTY = 'rsBreadcrumb';

@Injectable()
export class RsBreadcrumbService {
  private _current: BehaviorSubject<any> = new BehaviorSubject<any>({});
  readonly current: Observable<any> = this._current.asObservable();

  private config: RichSnippetConfig;

  constructor(
    private richSnippetService: RichSnippetService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {

    richSnippetService.config.subscribe(val => this.config = val);

    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route: ActivatedRoute) => {
        while (route.firstChild) { route = route.firstChild };
        return route;
      })
    ).subscribe((route) => this.routeChange(route.snapshot));
  }

  routeChange(routeSnapshot: ActivatedRouteSnapshot) {
    const breadcrumbs = [
      ...this.getHomeRouteBreadcrumb(),
      ...this.getBreadcrumbs(routeSnapshot)
    ].map(this.getListItem);

    const breadcrumbList = new BreadcrumbList({
      itemListElement: breadcrumbs
    });

    this._current.next(breadcrumbList.toJSON());
  }

  private getListItem(crumb, index) {
    return new ListItem({
      position: index + 1, item: { '@id': crumb.url, name: crumb.name }
    })
  }

  private getBreadcrumbs(routeSnapshot: ActivatedRouteSnapshot) {
    let urlSegments = [];
    return routeSnapshot.pathFromRoot
      .filter((route) => route.url.length > 0)
      .filter((route) => routeDataExists(DATA_PROPERTY, route))
      .map((route) => {
        urlSegments.push(route.url[0].path);
        return this.getBreadcrumb(
          getRouteData(DATA_PROPERTY, route),
          urlSegments.join('/') + '/'
        );
      })
  }

  private getBreadcrumb(routeData, path: string) {
    return {
      name: routeData.label,
      url: this.config.baseUrl + '/' + path
    }
  }

  getHomeRouteBreadcrumb() {
    return [
      this.getBreadcrumb( getRouteData(DATA_PROPERTY, this.getHomeRoute()), '') 
    ];
  }

  private getHomeRoute() {
    return this.router.config.find(route => route.path === '');
  }
}
